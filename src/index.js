import express from 'express';
import cors from 'cors';

const app = express();

const corsOptions = {
  origin: 'http://account.skill-branch.ru',
  optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
};

app.use(cors(corsOptions));

function transformName(string) {
	let shortName;
	let fullname = string.trim().replace(/\s+/ig, ' ');

	if(fullname.match(/[\/\d_]+/ig) || !fullname) {
		throw new Error('Invalid fullname');
	}

	fullname = fullname.split(' ');

	switch(fullname.length) {
		case 1:
			shortName = [capitaliseFirstLetter(fullname[0])];
			break;
		case 2:
			shortName = [
				capitaliseFirstLetter(fullname[1]),
				capitaliseFirstLetter(fullname[0], false)
			];
			break;
		case 3:
			shortName = [
				capitaliseFirstLetter(fullname[2]),
				capitaliseFirstLetter(fullname[0], false),
				capitaliseFirstLetter(fullname[1], false)
			];
			break;
		default:
			throw new Error('Invalid fullname');
	}

	return shortName;
}

function capitaliseFirstLetter(string, truncate = true) {
	let newString = string.charAt(0).toUpperCase();
	if (truncate) {
		newString += string.slice(1).toLowerCase();
	} else {
		newString += '.';
	}
    return newString;
}

app.get('/task2B', (req, res) => {
	try {
		let fullname = transformName(req.query.fullname).join(' ');
		res.send(fullname);

	} catch (err) {
		switch (err.message) {
			case 'Invalid fullname':
				res.send(err.message);
			default:
				res.sendStatus(500);
		}
	}
});

app.listen(3000, () => {
  console.log('Example app listening on port 3000!');
});
